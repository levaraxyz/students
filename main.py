import random
from classes.course import Course
from classes.student import Student
from classes.students_generator import StudentGenerator

def main(): 
    gen = StudentGenerator()
    students = gen.get_n(10)

    courses = [ Course("Programsko inzenjerstvo", "S303"),
                Course("Matematika 1", "S101") ]

    for student in students:
        student.enroll(random.choice(courses))

    for course in courses:
        course.print()

if __name__ == "__main__":
    main()
