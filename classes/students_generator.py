from classes.student import Student
import random

class StudentGenerator(object):

    """Docstring for StudentGenerator. """
    NAMES=["Pero", "Djuro", "Ana", "Marija", "Ivana", "Marijana", "Natasa", "Dzurdza"]
    LNAMES=["Peric", "Djuric", "Ivic", "Marijanovic", "Basic", "Ivancevic", "Lesak"]

    def __init__(self):
        pass
    def get_n(self, n):
        """returns n students in a list"""
        students = []
        for i in range(n):
            name = random.choice(self.NAMES)
            lname = random.choice(self.LNAMES)
            number = random.randint(1000, 3000)
            students.append(Student(name, lname, number))
        return students

if __name__ == "__main__":
    gen = StudentGenerator()
    students = gen.get_n(10)

    for i in students:
        print(i)
